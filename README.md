# Documentation OPAGA

Documentation d'OPAGA, extension WordPress de gestion administrative d'organisme de formation.

Pour en savoir plus sur OPAGA, consultez le site officiel : <https://opaga.fr>

Cette documentation est rédigée en Markdown et convertie en HTML à l'aide de [MdBook](https://rust-lang.github.io/mdBook/). Pour la reproduire vous devez donc installer MdBook ainsi que le plugin mkdocs-video :

```
cargo install mdbook
cargo install mdbook-admonish
cargo install mdbook-linkcheck
```

Pour créer une version locale :

```
mdbook serve
```

Pour créer la version à mettre en ligne (dans le dossier `book`) :

```
mdbook build
```
