# Formation en inter-entreprises

```admonish info
Bien que le terme contienne le mot *entreprise*, cela concerne tout type de client : associations, collectivités, particuliers, etc.
```

Vous organisez une session de formation pour laquelle vous avez plusieurs clients.

Chaque client vous confie un ou plusieurs stagiaires.

C'est la formule la plus lucrative mais la plus difficile à mettre en œuvre : vous devez traiter avec plusieurs clients à la fois avec chacun ses spécificités, ses modes de financements, etc.

```admonish note title="Dans la vraie vie"
Vous aurez probablement des demandes de personnes voulant suivre votre formation en inter, mais pas au dates ni dans le lieu que vous proposez (si vous avez annoncé quelque chose dans l'agenda).

Deux stratégies possibles :

* Proposer à votre prospect d'organiser une session en intra chez lui, discuter du fait que cela pourrait intéresser d'autres personnes qu'iel… Mais c'est plus cher !
* Proposer à votre prospect de fixer une date à au moins quatre mois de la demande initiale pour tenter d'organiser une session inter (éventuellement dans sa zone géographique). La condition de réalisation : il doit y avoir suffisamment de stagiaires inscrits. D'où les quatre mois de marge pour communiquer, mobiliser vos réseaux (locaux ou pas). Le prospect doit aussi mettre la main à la pâte et communiquer de son côté. Ça peut fonctionner.
```