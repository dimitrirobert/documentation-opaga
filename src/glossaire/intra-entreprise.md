# Formation en intra-entreprise

```admonish info
Bien que le terme contienne le mot *entreprise*, cela concerne tout type de client : associations, collectivités, particuliers, etc.
```

Vous n'avez qu'un seul client qui, à priori, vous a sollicité.

Il souhaite que vous formiez plusieurs personnes de son entreprise, son association, sa collectivité, etc.

Vous proposez un tarif de groupe et pouvez être souple sur le nombre de stagiaires.

Attention cependant, certains OPCO financent par stagiaire et non par groupe : un stagiaire manquant et c'est une partie du financement qui s'envole !