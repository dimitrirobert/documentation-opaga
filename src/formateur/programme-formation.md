# Rédigez un programme de formation

```admonish warning title="Ne pas confondre *formation* et *session de formation* !"

Ici vous apprendrez à rédiger un programme de *formation* pour le catalogue. Il n'y a pas de notion de date, de lieu, de clients ou de stagiaires. Ce n'est qu'une formation comme un produit « sur étagère ».

Cette formation sera publiée dans le catalogue public de votre organisme de formation, afin que les clients potentiels sachent qu'ils peuvent faire appel à vous sur ce sujet, même si aucune session n'est programmée !

Cette formation pourra ensuite être utilise comme base pour programmer des sessions de formation. Vous pourrez utiliser ce programme telle quelle ou la personnaliser.
```

## Catalogue du/de la formateur⋅trice

Depuis votre tableau de bord, dans l'onglet **Catalogue**, cliquez sur **Nouvelle formation**. Notez que vous pouvez modifier un programme de formation existant en cliquant sur le titre dans le tableau représentant votre catalogue de formations.

La colonne **Complétion** vous montre l'état d'avancement vers un programme complet et publiable. Passez la souris pour afficher, dans une infobulle, les noms des champs manquants.

La colonne **Visibilité catalogue** détermine si cette formation sera visible publiquement. Cette case à cocher est également disponible dans la page de modification d'une formation.

La colonne **Programmer une session** vous propose un raccourci pour créer une session à partir de cette formation. Pas d'ouverture de nouvelle fenêtre, vous êtes automatiquement affecté à l'équipe pédagogique et la page de gestion de la session est chargée.

![](/images/formation-catalogue-perso.png)

```admonish
Prévoyez du temps pour tout remplir, c'est un travail de longue haleine et fastidieux. Mais il faut bien le faire. Et il faut tout remplir.

Toutes les informations s'enregistrent automatiquement lorsque vous quittez le champ de saisie (on appelle cela une perte de focus).
```

## Informations administratives

### Titre de la formation

Donnez un titre précis, évocateur du contenu de votre formation. Il peut être long, mais pas trop. Il doit rester généraliste et attractif. Notez que vous pourrez le modifier dans une session si la personnalisation du parcours de formation vous y invite.

### Fiche publiée au catalogue

Cochez cette case pour que votre formation soit visible au catalogue. Si vous ne cochez pas cette case, cette fiche formation sera tout de même accessible pour programmer une session.

La question ici est seulement de savoir si vous souhaitez que cette formation apparaisse publiquement sur le site Web ou non.

### Spécialité

Appréciez le contenu de votre formation et trouvez la spécialité qui correspond le mieux. Si elle correspond à plusieurs spécialités, optez pour la catégorie englobante.

L'info-bulle donne des précisions sur la classification.

### Formateur⋅trices

Sélectionnez dans votre équipe pédagogique les personnes animant habituellement cette formation. Utilisez la touche *Ctrl* pour sélectionner plusieurs noms.

Il sera toutefois possible de programmer une session animée par une personne non sélectionnée ici.

### Durée prévue en heures

Indiquez ici la durée que vous prévoyez pour le suivi d'une session de cette formation. La durée doit être saisie en décimal : pour une heure trente, indiquez 1,5.

Rien ne vous empêche par la suite de programmer des sessions plus longues ou plus courtes selon les désirs de vos clients, mais en restant réaliste par rapport au parcours prévu.

### Tarifs

Vous devez à minima mentionner le tarif par stagiaire. Saisissez donc un tarif horaire ou total. Vous devez avoir renseigné la durée si vous souhaitez que le calcul soit automatique.

L'époque où la journée de formation durait forcément sept heures étant révolue, il n'y a pas de tarif journalier.

Vous pouvez indiquer un tarif de groupe à destination des clients souhaitant former plusieurs personnes.

Si vous le définissez dans les options d'OPAGA vous pouvez préciser d'autres tarifs (par exemple, un tarif réduit pour les particuliers, les membres de votre association, etc.)

Ces tarifs sont indiqués dans le catalogue mais ne vous contraignent aucunement. Vous pouvez programmer des sessions avec un tarif public différent. De même, vous négocierez un tarif avec vos clients qui sera peut-être encore différent (même au sein d'une même session).

## Parcours pédagogique

### Présentation générale


Présentation commerciale de la formation. Elle est succincte mais on doit comprendre de qui il s'agit.

Pas de formalisme administratif, elle doit convaincre les clients de vous contacter.

### Objectifs professionnels

Objectifs généraux, qu'est-ce que cette formation va apporter aux stagiaires dans leur quotidien professionnel.

### Objectifs pédagogiques

Indiquez ici les compétences à acquérir entraînant un changement dans les savoirs, les savoir-être et/ou les savoir-faire des stagiaires. Chaque objectif à évaluer est décomposé en trois parties.

* Un verbe d'action qui décrit un comportement observable. Vous pouvez vous inspirer de la taxonomie de Bloom.
* La ou les contraintes imposées (moyen, lieu, méthode) au stagiaire pour atteindre l'objectif.
* Les critères de réussite qualitatifs ou quantitatifs, tels qu'un pourcentage d'erreur, une durée limitée.

Chaque objectif représente un but à atteindre, une acquisition de compétence. Il doit être suffisamment précis pour vous permettre de l'évaluer facilement, mais pas trop pour ne pas vous engager sur des objectifs intenables.

### Pré-requis

Indiquez précisément les compétences et/ou le niveau requis pour suivre cette formation. Si cette formation n'a **pas de pré-requis**, il faut tout de même l'indiquer.

C'est aussi une garantie pour le formateur que de préciser ce qui est indispensable avant l'entrée en formation des stagiaires.

### Public concerné

À quel type de public s'adresse cette formation. Évitez le « tout public » essayez d'être un peu précis, mais pas trop pour ne pas en restreindre l'accès inutilement.

### Modalités pédagogiques

Précisez votre méthode et vos techniques pédagogiques. Comment vous échangez avec vos stagiaires, qu'est-ce que vous leur demandez, la part de théorie et de pratique, comment se passe la pratique, etc.

Distinguez ce qui se passe en présence et à distance, si plusieurs modes sont possibles.

### Ressources pédagogiques

Y a-t-il des supports pédagogiques, des ressources à utiliser pendant la session ?

Indiquez ici tout le contenu que vous transmettez et laissez à vos stagiaires en amont, pendant et en aval de la session.

### Modalités d'organisation

Indiquez ici la part de présence et de distance.

Indiquez également la durée (même si elle est précisée par ailleurs) et comment se répartissent les heures de formation dans le temps (pas forcément des journées de 7h).

Indiquez aussi le degré de personnalisation du contenu pour les stagiaires.

### Matériel pédagogique

Indiquez le matériel utilisé et celui nécessaire pour chaque stagiaire.

Qui fournit le matériel ? Disposez-vous d'une salle équipée ? Avez-vous une batterie d'ordinateur que vous pouvez déployer chez le client ?

Par exemple, en présence vous aurez peut-être besoin d'un vidéo-projecteur et que chaque stagiaire soit muni d'un ordinateur.

De même, à distance, quels moyens techniques mettez-vous en œuvre ? Un logiciel de visio-conférence, une plateforme pédagogique, un pad, un partage de fichiers, etc.

### Accessibilité

Comment prenez-vous en compte les personnes souffrant de handicap ? Disposez-vous d'une salle équipée et accessible ?

Sinon, comment peuvent se manifester les stagiaires concernés en amont de la session ?

### Modalités d'inscription et délai d'accès

Qui est l'interlocuteur des clients et stagiaires ? Comment se déroule la procédure entre le désir de suivre une formation et l'inscription ferme ? Prise de contact, entretien, etc.

Quels délais sont à prévoir ? Dans le cas d'un financement par un OPCO (prévoir au moins un mois) ? un financement sur fonds privés ?

### Programme

Programme de votre parcours de formation.

Vous pouvez le découper en jours, en modules. Il faut que ce soit clair et précis.

Cependant, n'oubliez pas que le programme (comme le reste) vous engage, ne vous emballez pas trop !

### Évaluation des pré-requis et des objectifs pédagogiques

Comme dit précédemment, vous devez évaluer les compétences de vos stagiaires avant et après la session. Ici vous pouvez créer des questionnaires simples : mentionnez une suite de compétences (une par ligne) et cela crée une auto-évaluation où les stagiaires indiquent leur niveau de maîtrise entre 1 et 5.

C'est forcément imparfait, limité, donc n'hésitez pas à utiliser d'autres solutions comme [Framaforms](https://framaforms.org) (questionnaire libre en ligne basé sur [YakForms](https://yakforms.org/)), [LimeSurvey](https://www.limesurvey.org/fr/) (logiciel d'un niveau avancé pour conduire des études statistiques), un LMS (*learning managing system* ou plateforme pédagogique) tel que [Moodle](https://moodle.org/) qui permet de créer des questionnaires.

### Propriété intellectuelle

Au cours de vos actions de formation vous allez transmettre des contenus. Indiquez ici les droits dont disposeront éventuellement les stagiaires. Cette information peut être reprise dans la convention de formation et le contrat de formation à titre individuel.

```admonish info title="Autres paramètres propres à WordPress"

Les autres paramètres tels que **Catégories**, **Étiquettes** et **Image mise en avant** qui étaient présents à l'époque de la création/édition en backend ne sont pas utilisés par OPAGA et ne sont plus disponibles ici.

En effet, vous pouvez choisir de les utiliser sur votre site Web pour proposer un classement thématique de votre catalogue de formations.

Les étiquettes et les catégories sont accessibles uniquement pour les responsables de formation : il convient de les attribuer en maintenant la cohérence de l'organisme de formation.

Inutile de déclarer une catégorie ou une étiquette « formation ». Les formations ainsi saisies sont d'un type de contenu personnalisé `formation`. Si vous souhaitez personnaliser votre thème vous pouvez identifier les formations en testant la valeur `post_type` du `post` courant.
```