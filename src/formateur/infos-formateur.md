# Profil de formateur⋅trice

Dans le cadre de l'indicateur 1 de Qualiopi vous devez informer vos clients sur les prestations proposées. Cela passe, entre-autres, par l'information sur les intervenants.

Les informations saisies dans cet onglet seront reprises dans le document de proposition de formation envoyé au client avec le devis.

* **Photo** et **CV** : parcourez votre disque local et déposez une image pour la photo, un document PDF pour le CV.
* La **photo** n'est pas obligatoire.
* Le **CV** est obligatoire, mais vous pouvez mettre un lien vers un CV en ligne plutôt que de déposer un PDF. Si vous renseignez les deux champs (inutile) le PDF sera ignoré. Attention, un lien vers votre profil LinkedIn ne sera visible que par les abonnés à ce service. Vous devez fournir un CV public !
* L'**activité** doit être décrite de manière concise et précise concernant le métier du⋅de la formateur⋅rice. Ce champ peut être utilisé dans la convention.
    * Dans un contexte de CAE (coopérative d'activités et d'emploi), les coopérateurs peuvent avoir leur propre **marque**, différente du nom de la coopérative. Vous pouvez faire apparaître cette marque dans vos documents.
* La **présentation** succincte vous permet d'indiquer, par exemple, des éléments d'expérience qui montrent que vous connaissez votre sujet de formation.
* Les **réalisations** (champ totalement libre et non obligatoire). Mettez ici des choses que vous souhaitez montrer au public.

![](/images/profil_formateur.png)