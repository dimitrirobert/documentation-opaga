# Tableau de bord

Le tableau de bord est la page sur laquelle vous arrivez lorsque vous vous connectez. Il reprend les informations importantes pour les formateur⋅ices.

* [Agenda](../session/programmer-session.md) : retrouvez rapidement les listes de vos sessions en cours, futures et passées. D'ici vous avez également le bouton pour créer rapidement une session en sous-traitance sans passer par le catalogue.
* [Catalogue](../formateur/programme-formation.md) : liste de vos programmes de formation. À partir d'un programme de formation vous pouvez programmer une session.
* [Profil de formateur⋅ice](./infos-formateur.md) : votre profil à remplir, certains champs sont obligatoires du point de vue de Qualiopi et pour promouvoir vos prestations depuis le site public.
* [Veille](./veille.md) : un bloc-notes pour noter votre veille.