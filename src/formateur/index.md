# Vous êtes formateur⋅trice

## Soignez votre profil

Au début vous devez renseigner un certain nombre d'informations. À priori, vous n'aurez à le faire qu'une seule fois, mais vous devrez vous assurer que ces informations sont toujours à jour (notamment votre CV).

* [Renseigner votre fiche de formateur](./infos-formateur.md)
* [Rédiger des programmes de formation](./programme-formation.md)

## Gérez vos sessions, clients, stagiaires

Votre gestion courante dans OPAGA concerne votre activité.

* [Programmer une nouvelle session](../session/programmer-session.md)
* [Gérer les clients](../client/index.md)
* [Gérer les stagiaires](../stagiaire/index.md)
* [Produire les documents](../document/index.md)
* [Finaliser la session](../session/finaliser.md)
