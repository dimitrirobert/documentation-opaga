# Veille

L'onglet **Veille** vous permet de noter des éléments de votre veille, particulièrement la **veille métier (indicateur 24)**.

Les indicateurs 23, 24 et 25 du critère 6 de Qualiopi précisent que les formateur⋅rices réalisent trois types de veille :

23. *Le prestataire réalise une veille **légale et réglementaire** sur le champ de la formation professionnelle et en **exploite les enseignements***.

> Cette veille est réalisée par l'organisme de formation et transmise aux intervenants.

24. *Le prestataire réalise une veille sur les évolutions des **compétences**, des **métiers** et des **emplois** dans ses **secteurs d’intervention** et en **exploite les enseignements***.

> Cette veille doit être effectuée par les formateur⋅ices, qui doivent se tenir au courant des évolutions de leurs domaines de compétence et adapter leurs prestations.
> 
> Veuillez prendre soin de noter vos actions et les enseignements que vous en tirez, **même ce qui vous paraît implicite**.

25. *Le prestataire réalise une veille sur les **innovations pédagogiques et technologiques** permettant une évolution de ses prestations et en **exploite les enseignements***.

> Cette veille peut être réalisée par l'organisme de formation et transmise aux intervenants et/ou par les intervenants aux-mêmes et partagée à l'équipe pédagogique.

