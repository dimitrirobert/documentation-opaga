# Gestion des sessions

## Informations indispensables à renseigner dès le début

Pour que votre session soit publiable vous devez au minimum :

* dans l'onglet **Dates** [définir une ou des dates](./planning.md) ainsi que les créneaux horaires (les dates et créneaux peuvent être modifiés à posteriori) ;
* dans l'onglet **Lieu** définir un lieu où va se dérouler la session (peut également être modifié à posteriori) ;
* si vous avez choisi une formation du catalogue, vérifiez dans l'onglet **Formation** que toutes les informations sont bien présentes et correctes ;
* si vous avez créé une session unique sans lien avec le catalogue, vous devez remplir tout l'onglet **Formation** (reportez-vous à [rédigez un programme de formation](../formateur/programme-formation.md) pour les détails) ;
* dans l'onglet **Session** et dans le cas où votre session est publiée dans l'agenda, fixez un prix public par stagiaire, ainsi que la fourchette de stagiaires que vous acceptez et assurez-vous d'avoir au moins un formateur. Si vous proposez une session privée (en intra-entreprise ou en sous-traitance par exemple), ces informations ne sont pas utilisées.

Il n'y a pas de notion de session [inter](../glossaire/inter-entreprises.md), [intra](../glossaire/intra-entreprise.md)) ou en sous-traitance. C'est le nombre et/ou la typologie des clients qui gère cet aspect.

À partir de là, vous pouvez éventuellement rendre publique la session (si vous êtes dans une logique de session inter-entreprises) ou [ajouter votre premier client](../client/index.md) dans l'onglet **Clients/stagiaires**.

## Vérifier le contenu de la formation

Dans l'onglet **Formation** vous trouverez tout ce que vous avez déjà défini dans le programme de formation du catalogue. Cela vous sert de base. Vous êtes libre d'adapter chacun des champs en fonction de votre session, voire de votre unique client.