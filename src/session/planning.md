# Définir le planning

Dans l'onglet **Dates** vous indiquez les dates et les créneaux horaires de votre session.

```admonish
Si cette session est réalisée **entièrement en sous-traitance**, inutile de préciser les créneaux, les dates suffisent. Vous renseignerez la durée au niveau de la fiche client.
```

## Ajoutez une date

Cliquez sur le bouton **+ Date** et choisissez la première date dans le calendrier qui s'affiche.

## Ajoutez un créneau

Une fois la date créée, utilisez le bouton **+ Créneau** et définissez-le (heure de début et de fin, type de créneau).

```admonish warning title="Attention"
Les créneaux **ne** doivent **pas** contenir les temps de repas. Pour une journée *classique* en salle, définissez **deux** créneaux, un pour le matin, un pour l'après-midi !
```

<video controls src="/video/ajouter-date-creneaux.mp4"></video>

## Dupliquez les dates et créneaux

Généralement les journées de sessions de formation se ressemblent dans leur organisation (et c'est mieux pour les stagiaires), donc utilisez les boutons **Copie + 1j** et/ou **Copie + 7j** pour dupliquer la date et ses créneaux respectivement le lendemain et la semaine suivante.

Vous pouvez modifier les dates à posteriori, de même que les créneaux. Cette fonction de copie de date n'est là que pour faciliter la saisie.

## Décalez toute la session

Il arrive souvent, lorsque le nombre de stagiaires n'est pas suffisant, que vous choisissiez de décaler votre session à une période ultérieure.

Si votre session comporte plusieurs dates (à partir de deux) vous disposez alors du champ **Décaler la première date** qui ouvre un nouveau calendrier. Choisissez la nouvelle première date puis validez.

Toutes vos dates sont alors décalées en conservant les mêmes intervalles. Vous pouvez être amenés à rectifier certaines qui tomberaient le week-end (le jour de la semaine en mentionné pour chaque date) ou un jour férié (prochainement les jours fériés français seront intégrés).

![](/images/session-decaler.png)