# Programmez une session de formation

```admonish important
Étape obligatoire pour déclencher une action de formation, vous devez la créer.
```

## Cas classique

Cela se fait depuis votre tableau de bord, dans l'onglet **Catalogue**.

Généralement vous allez créer une session à partir d'un programme de formation. Depuis le tableau de vos formations cliquez sur le bouton avec une horloge dans la colonne **Programmer une session**. Cela crée la session et vous envoie directement sur sa page de gestion.

![](/images/nouvelle-session-depuis-catalogue.png)

## Hors catalogue ou sous-traitance

Si vous créez une session sans le support d'un programme de formation (pour une session unique que vous ne reproduirez pas ou si vous êtes uniquement sous-traitant), utilisez le bouton **Nouvelle session**.

Cela vous ouvre une fenêtre vous demandant des renseignements minimalistes.
Indiquez un titre et validez.

![](/images/boite-nouvelle-session.png)

La page se recharge ensuite sur la session nouvellement créée.
