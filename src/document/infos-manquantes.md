# Contrôle des informations manquantes

Une fois le brouillon créé, une pastille vous informe de la complétion ou non de votre document.

* Si la pastille est verte avec une coche à l'intérieur, tout va bien, c'est complet, vous pouvez créer la version finale.
* En revanche, si la pastille est rouge avec un nombre à l'intérieur, ce nombre vous indique le nombre de champs incomplets. Cliquez sur la pastille rouge pour ouvrir la boîte de messages, en bas de l'écran, qui vous renseigne que les champs manquants (et que vous devez compléter avec de finaliser le document ou d'en demander la signature).

![](/images/gestion-docs-infos-manquantes.png)

Vous pouvez également visionner le PDF : les champs manquants sont remplacés par leur description sur fond noir.

![](/images/gestion-docs-infos-manquantes-pdf.png)

```admonish warning title="Attention"
Si malgré tout vous assumez les champs manquants, vous pouvez tout de même produire le document final. Tous les manques sont alors ignorés et laissés blancs dans le PDF.
```