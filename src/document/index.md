# Gérez les documents administratifs

![](/images/gestion-docs-client.png)

Un certain nombre de colonnes vous permettent d'agir sur chacun des documents :

* **Brouillon** : commencez toujours par créer un brouillon. Cliquez sur **Créer**, par exemple, sur la ligne de la convention de formation professionnelle. Le bouton se change en **Mettre à jour** vous indiquant que vous pouvez créer à nouveau le document à tout moment.
Vous pouvez télécharger la convention : 

![](/images/gestion-docs-client-brouillon.png)

* **Final** : une fois votre brouillon relu vous pourrez créer la version finale, sauf si le document nécessite la signature de votre responsable de formation.
* **Signature responsable** : si le document nécessite la signature de votre responsable, le bouton **Demander** lui signale que vous souhaitez une signature. **Attention** un mail est envoyé à chaque responsable ayant pour tâche la signature des documents et il apparaît dans leur [tableau de bord](../responsable/valider-document.md).
* **Diffuser** : une fois votre document finalisé, vous pouvez le diffuser à votre client. Il apparaîtra dans son tableau de bord. Et un email lui sera envoyé lorsque cette fonction sera implémentée.
* **Scan** : vous avez la possibilité de déposer ici un scan ou une version signée numériquement par un tiers (par exemple, par le client).
* **Supprimer** à n'utiliser qu'en dernier recours, vous devrez alors tout recommencer.

``` admonish note title="Signature"
Quatre entités peuvent être amenées à devoir signer un document :
    
* le⋅la (ou les) responsable de formation, représentant l'organisme de formation en tant que prestataire ; les documents concernés ne peuvent être finalisés par le⋅la formateur⋅trice (seul le brouillon est possible) et doivent être validés par un responsable.
* le client (donc par le⋅la représentante de la personne morale cliente).
* le⋅la stagiaire.
* le⋅la formateur⋅trice.
	
Un document peut être signé par plusieurs entités. Ces entités signataires sont définies au niveau du [modèle de document](../modele/index.md).
```

```admonish warning title="Attention"
Les documents PDF ne sont pas mis à jour automatiquement lorsque vous complétez des informations : c'est à vous de les créer à nouveau avec le bouton **Mettre à jour** !
```

- [Personnalisez certains paramètres](./param-personnalises.md)
- [Repérez les informations manquantes](./infos-manquantes.md)
