# Complétez les paramètres personnalisés

Un modèle de document est constitué de texte et de mots-clés qui sont remplacés par la valeur attendue lorsque vous créez le document. OPAGA offre aux responsables de formation d'ajouter des mots-clés personnalisés, non prévus dans OPAGA. Comme ils ne sont pas reliés à la base de données, c'est à vous de renseigner les valeurs attendues.

Si vous voyez un crayon à gauche du bouton Créer de la colonne Brouillon, alors ce document comporte des champs personnalisés.

![](/images/gestion-docs-mots-cles-personnalises-crayon-orange.png)

Cliquez sur le crayon pour ouvrir le formulaire qui vous permettra de les renseigner.

![](/images/gestion-docs-mots-cles-personnalises-formulaire.png)

Renseignez tous les champs. Cela peut être un simple *Oui/Non*, un texte ou un nombre.

Certains champs peuvent avoir une valeur par défaut. C'est une information saisie par votre responsable de formation qui peut vous aider, surtout lorsque le champ est un texte. Cliquez sur le bouton **Valeur par défaut** pour la copier dans la zone de texte.

Enfin validez et constatez que le crayon a viré au vert, signifiant que tous les champs personnalisés sont bien renseignés.

![](/images/gestion-docs-mots-cles-personnalises-crayon-vert.png)

Vous pouvez à nouveau cliquer sur le crayon pour modifier ces champs si nécessaire. Ils sont désormais enregistrés dans le document, vous n'avez pas besoin de les saisir à chaque création de **ce** document. En revanche, vous devez saisir ces informations pour tous les autres documents basés sur le même modèle.

