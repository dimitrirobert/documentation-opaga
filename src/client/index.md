# Gérer un client

Vous pouvez créer autant de clients que vous le souhaitez. Il existe deux types de client spécifique :

* Le particulier est une personne physique : la procédure est différente d'une personne morale et il est également l'unique stagiaire de ce client.
* L'organisme de formation qui organise la session : vous agissez alors en sous-traitance. La procédure est grandement allégée, vous ne devez fournir que quelques informations pour le bilan pédagogique et financier, votre client s'occupant de toute l'organisation.

Hors de ces deux cas de figures vous aurez affaire à une personne morale.

```admonish info title="Inter ou intra ?"

* si vous n'avez qu'un seul client et que vous n'êtes pas sous-traitant, alors vous organisez une session [**intra-entreprise**](../glossaire/intra-entreprise.md) ;
* si vous avez plusieurs clients, vous organisez une session [**inter-entreprises**](../glossaire/inter-entreprises.md) ;
* rien ne vous empêche d'être en sous-traitance pour un autre organisme de formation et d'avoir d'autres clients gérés en direct.
```

## Sommaire

- [Créez un client](./nouveau-client.md)
- [Le tableau de bord client](./tableau-de-bord.md)
- [Sources de financement](./financement.md)
- [Gérez les documents administratifs](./docs-admin.md)
- [Créneaux contractualisés](./creneaux.md)
- [Le client est un particulier](./particulier.md)
- [Vous êtes sous-traitant](./sous-traitance.md)
