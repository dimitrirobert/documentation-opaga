# Le client est un particulier

![](/images/tableau-client-particulier.png)

Lors de la création d'un nouveau client, vous pouvez choisir de créer un particulier. C'est le choix à faire lorsque votre client n'a pas de numéro de Siret.

Un client particulier est également l'unique stagiaire. Aussi les tableaux client et stagiaire sont fusionnés pour ne faire apparaître, sur un seul tableau, que les informations nécessaires.

## Informations personnelles

Le strict minimum est demandé :

* Prénom, nom, adresse postale pour établir la facture.
* Numéro de téléphone et/ou adresse email pour échanger, gérer la relation.

## Financement

Ses modes de financement sont limités. Cela peut être dans certains cas

* promotion ou reconversion en alternance ([Pro-A](https://www.service-public.fr/particuliers/vosdroits/F13516)) si le stagiaire n'est pas en contrat de travail

ou

* congé individuel de formation ou projet de transition professionnelle
* compte personnel de formation
* dispositif spécifique pour les personnes en recherche d'emploi

ou encore

* Contrats conclus avec des personnes à titre individuel et à leurs frais.

## Échéancier

Habituellement vous demandez un acompte avant le début de la session. L'acompte ne peut dépasser les 30% du montant total. L'acompte est également un engagement juridique à payer le reste du montant.

Vous devez rédiger un échéancier de paiement. Le champ **Échéancier** est un champ libre qui vous permet de détailler le calendrier de paiement.
Cet échéancier sera automatiquement reporté dans le *contrat de formation professionnelle à titre individuel* le document vous permettant de contracter avec un particulier.

L'acompte n'a pas à être mentionné dans l'échéancier, il est géré par ailleurs dans le document. En revanche, vous devez au moins indiquer le solde.

```admonish warning title="Attention"
Précisez bien les montants HT et TTC afin d'éviter toute confusion. Pour vous aider dans vos calculs, la zone **Pour informations** détaille les montants HT et TTC en fonction des choix de TVA de votre organisme de formation.
```

## Informations concernant le stagiaire

Les informations telles que le statut du stagiaire sont identiques à celles que l'on trouve dans une fiche stagiaire classique.

Les évaluations et le recueil des besoins également.

Consultez la page [Gérer les stagiaires](../stagiaire/tableau-de-bord.md) pour plus de détails.

## Documents

Dans la liste des documents vous devez pouvoir créer le contrat de formation professionnelle à titre individuel. S'il n'apparaît pas, demandez à votre responsable de formation de le rendre disponible (le modèle doit être disponible pour les *contrats en direct, personne physique* et pour l'entité *client*).

