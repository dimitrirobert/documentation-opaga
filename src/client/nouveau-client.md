# Créer un nouveau client

Pour ajouter un client, cliquez dans l'onglet **Clients/stagiaires**, puis sur **Ajouter client**.

![Fenêtre nouveau client](/images/nouveau-client.png)

Deux cas particuliers sont gérés ici :

* le client est [un autre organisme de formation](#le-client-organise-la-session-de-formation) (vous agissez en sous-traitant)
* le client est [un particulier](#le-client-est-un-particulier)

En dehors de ces deux cas de figure, choisissez **Toute autre personne morale**.

Renseignez le *nom ou raison sociale* et cliquez sur *Valider*.

