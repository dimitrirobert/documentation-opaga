# Créneaux contractualisés

Parfois le client vous demande un allègement de la formation, parce que son ou ses stagiaires maîtrisent déjà certaines parties, certains modules. C'est à vous de déterminer, en concertation avec le client, si vous accordez un tel allègement.

Dans ce cas, vous pourrez décocher les créneaux auxquels le client ne souscrit pas, cela recalcule le temps de formation et vous permet de créer les feuilles d'émargement adaptées.

![](/images/client-creneaux-contractualises.png)
