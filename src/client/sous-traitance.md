# Le client organise la session de formation

Si votre client est lui-même un organisme de formation, alors vous êtes sous-traitant. Les informations à saisir sont beaucoup plus réduites (comme votre chiffre d'affaires d'ailleurs).

Dans l'identité du client vous devez préciser son numéro de déclaration d'activité auprès de la Dreets de sa région (parfois appelé numéro d'organisme de formation). Vous pouvez trouver vous-même cette information (et au passage vous assurer que vous avez bien à faire à un organisme de formation) dans la [liste du gouvernement](https://www.data.gouv.fr/en/datasets/liste-publique-des-organismes-de-formation-l-6351-7-1-du-code-du-travail/).

La partie financière doit être remplie avec :

* le tarif total
* d'éventuels frais non pédagogiques
* le nombre de stagiaires
* le nombre d'heures de la session
* le nombre d'heures × stagiaires qui n'est pas forcément le produit des deux précédentes valeurs ! Pensez à décompter les absences éventuelles de stagiaires à certains créneaux.

![](/images/tableau-client-finance-sous-traitance.png)

Si vos responsables de formation ont créé un modèle de document pour le **contrat de sous-traitance**, vous le trouverez dans la partie **Documents administratifs pour le client**. Vous pourrez alors déposer ce contrat, en demander la signature par votre responsable.

Vous pourrez exceptionnellement créer ce document à la demande de votre client, ou si votre client ne le fournit pas. Mais souvenez-vous que ce document doit émaner de votre client !

Si vous ne disposez pas de ce modèle de document vous pouvez toujours déposer le document dans la zone de téléchargement libre tout en bas de l'onglet **Session**.

![](/images/session-televersement.png)