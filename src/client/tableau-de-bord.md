# Tableau de bord client

Le tableau de bord vous montre toutes les informations concernant le client.
Vous devez remplir :

* numéro de contrat en interne dans votre organisme (champ additionnel à (dés)activer dans les [options](../params/opt-opaga.md#champs-additionnels))
* adresse postale,
* code postal,
* ville,
* téléphone,
* pays (si différent de la France),
* numéro de RCS (registre du commerce et des sociétés),
* numéro de Siret,
* responsable signataire des documents administratifs
	* prénom et nom
	* adresse email,
	* numéro de téléphone
* contact commercial (si différent du responsable)
	* prénom et nom,
	* adresse email,
	* numéro de téléphone

```admonish important
Les adresses email sont importantes pour transmettre le lien d'accès privé à votre client !
	
De plus, il peut être nécessaire de disposer du numéro de téléphone de la personne responsable dans le cas d'une signature électronique.
```

