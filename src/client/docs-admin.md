# Documents administratifs client

Dans la partie **Documents administratifs pour le client** sont listés les documents concernant le contexte « client », c'est-à-dire que vous en aurez un pour chaque client. La liste de ces documents est gérée au niveau de chaque organisme de formation dans la section [Modèles](../modele/index.md) dans le *backoffice*.

![](/images/gestion-docs-client.png)

Voir comment [gérer les documents administratifs](../document/index.md).

