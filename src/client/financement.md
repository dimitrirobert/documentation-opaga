# Options importantes pour le bilan pédagogique financier 

![](/images/tableau-client-finance.png)

## Principale source de financement

Pour un particulier utilisant ses frais personnel, il faut sélectionner l'option *Contrats conclus avec des personnes à titre individuel*.

La liste des options provient de ce [CERFA en page 1](https://www.formulaires.service-public.fr/gf/getNotice.do?cerfaNotice=50199&cerfaFormulaire=10443).

```admonish info title="Sources de financement" collapsible=true

* Entreprise pour la formation de ses salariés
* Organismes paritaires collecteurs ou gestionnaires des fonds de la formation
    * contrat d'apprentissage
    * contrat de professionnalisation
    * promotion ou reconversion en alternance
    * congé individuel de formation ou projet de transition professionnelle
    * compte personnel de formation
    * dispositif spécifique pour les personnes en recherche d'emploi
    * dispositif spécifique pour les travailleurs non-salariés
    * plan de développement des compétences ou autres dispositifs
* Pouvoirs publics pour la formation de leurs agents (État, collectivités territoriales, établissements publics à caractère administratif)
* Pouvoirs publics pour la formation de publics spécifiques
    * Instances européennes
    * État
    * Conseils régionaux
    * Pôle emploi
    * Autres ressources publiques
* Contrats conclus avec des personnes à titre individuel et à leurs frais
* Contrats conclus avec d’autres organismes de formation
* Autres produits au titre de la formation professionnelle continue
```

## Objectif de la prestation

Explications détaillées dans le même CERFA ci-dessus.

Choisissez « autre formation professionnelle continue » si aucun autre choix ne convient. C'est celui à choisir lorsqu'il n'y a aucun cadre particulier.

```admonish info title="Objectifs de la prestation" collapsible=true

* Autre formation professionnelle continue
* Bilan de compétences
* Actions d'accompagnement à la validation des acquis d'expérience
* certification (dont CQP) ou habilitation enregistrée au répertoire spécifique (RS)
* CQP non enregistré au RNC ou au RS
* Diplôme ou titre à finalité professionnelle (hors CQP) inscrit au RNCP
    * Niveau 6 à 8 (licence, master, diplôme d’ingénieur, doctorat)
    * Niveau 5 (BTS, DUT, écoles de formation sanitaire et sociale…)
    * Niveau 4 (BAC professionnel, BT, BP, BM…)
    * Niveau 3 (BEP, CAP,…)
    * Niveau 2
    * certificat de qualification professionnelle (CQP) sans niveau de qualification
```

## Tarif 

Indiquez le tarif total négocié avec le client (c'est le montant que vous allez facturer !). Trois cas de figures sont possibles mais un seul choix doit être fait dans les [options](../params/opt-of.md#tva) :

* votre OF n'est pas assujetti à la TVA (association, micro-entreprise), vos tarifs ne comportent pas de TVA et sont considérés TTC ;
* votre OF est assujetti à la TVA (entreprise), vos tarifs sont hors taxe et le montant TTC est précisé en sus ;
* votre OF est assujetti à la TVA (entreprise) mais vous êtes exonéré de TVA sur l'activité de formation (article 261 du CGI) : vos tarifs sont hors taxe et il n'y a de TVA que sur les frais non-pédagogiques.

Si vous avez défini des créneaux horaires une fenêtre s'affichera à droite **Pour information** avec le total du nombre d'heure, le tarif horaire.

### Acompte

Vous pouvez définir un acompte dont le client devra s'acquitter avant le début de la session. Le bouton indiquant **30 %** sur la capture permet de calculer automatiquement un acompte du pourcentage indiqué, mais vous pouvez changer cette valeur à la main.

À noter que le pourcentage proposé est paramétrable dans les options d'OPAGA par votre responsable de formation.

