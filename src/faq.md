# Questions plus ou moins fréquentes

- [Qu'est-ce qu'une formation ?](./faq/01-quest-ce-quune-formation.md)                                                                                                                                           
- [Qu'est-ce qu'une session ?](./faq/02-quest-ce-quune-session.md)                                                                                                                                               
- [Je ne travaille qu'en sous-traitance, que dois-je remplir ?](./faq/03-sous-traitance-seulement.md)                                                                                                            
- [Lors d'un contrat avec un client qui vous confie plusieurs groupes à des moments différents, dois-je créer plusieurs sessions ?](./faq/50-client-multi-groupe.md)
- [J'ai perdu ma session !](./faq/50-session-perdue.md)
