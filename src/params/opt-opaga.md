# Options d'OPAGA

Deuxième onglet de la page OPAGA du backend de WordPress.

![](/images/options-opaga.png)

## Première année d'activité

Par défaut est mentionnée ici l'année d'installation d'OPAGA. Cela sert à afficher la liste des années d'activité sur les pages BPF et pilote de sessions. À priori, vous ne devriez pas avoir à modifier cette valeur.

## Pages spéciales

Un certain nombre de pages spéciales (définies virtuellement dans WordPress) pour des usages transversaux. Vous pouvez paramétrer l'URL d'accès (qui s'ajoute à l'URL de votre WordPress) et le titre.

## Champs additionnels

Selon les usages divers des organismes de formation certaines paramètres peuvent être utile, mais pas à d'autres. Cette liste a vocation à grandir en fonction des retours des utilisateurs.

* **Numéro de contrat pour les clients** : si vous numérotez vos contrats en interne vous pourriez avoir envie de les reporter dans OPAGA. Cette information est utilisée pour trier les sessions dans le pilote de sessions.
* **Lors de la création d'un document à signer, possibilité de préciser une date différente de celle du jour** : parfois l'on doit antidater une signature. Or, les documents à signer comporte toujours la date du jour (mot-clé `{today}`). Cette option vous permet d'outrepasser le comportement normal. À utiliser avec parcimonie.
* **Les formateurs peuvent avoir et mettre en avant leur propre marque** : dans un contexte de CAE (coopérative d'activités et d'emplois), les formateur⋅trices peuvent avoir leur propre marque et vouloir la mettre en avant, en plus de leur nom et prénom.
* **Enregistrer le genre des stagiaires** : à des fins de statistiques relatives à des financements publics, il peut être nécessaire de connaître le genre des stagiaires.
* **Permettre d'inscrire des stagiaires aléatoires** : à des fins de tests cela vous permet de palier au manque d'inspiration lorsqu'il faut trouver des noms imaginaires.

## Durée de validité des jetons d'accès

Les clients et les stagiaires accèdent à OPAGA via un lien privé composé d'un jeton d'accès. Vous pouvez ici choisir la durée (en heures) de validité maximale après premier accès. Si vous indiquez 0 les jetons seront valides sans limite.
