# Feuilles d'émargement

Le cas des feuilles d'émargement est particulier. En effet, vous définissez un modèle qui peut se reproduire sur plusieurs pages.

L'emploi d'un mot-clé tel que `client:emargement_collectif` entraîne le calcul du nombre de pages et la duplication du modèle afin de répartir tous les stagiaires et créneaux horaires sur plusieurs pages.

