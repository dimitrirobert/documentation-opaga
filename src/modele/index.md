# Modèles de documents

Un modèle de document se compose de trois parties :

* en-tête
* corps
* pied de page

L'en-tête et le pied sont à créer une seule fois. Le corps sera propre à chaque type de document.

## Un nouveau modèle

Le corps du modèle se défini en créant un contenu de type **Modèle** dans le sous-menu d'OPAGA à gauche.

![](/images/modele-nouveau.png)

Le titre est celui qui sera utilisé ensuite dans OPAGA pour désigner le document. Il n'est pas intégré automatiquement dans le document, c'est à vous de choisir son emplacement en utilisant le mot-clé `document:titre`.

## En-tête et pied de page

Ces deux parties se définissent dans les options générales puis l'onglet **[Options PDF](../params/opt-pdf.md)**.

![](/images/modele-entete-pied.png)

Hormis les mots-clés personnalisables qui n'ont pas d'utilité pour ces parties, la rédaction de l'en-tête et du pied de page est identique à celle du contenu.