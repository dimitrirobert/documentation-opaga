# Insérez des lignes formatées

Le menu **Format** de l'éditeur propose d'insérer des lignes préformatées :

* **Ligne flexible** : comme une ligne de tableau mais la largeur des cases s'adapte au contenu.
* **Ligne de signatures** : identique à la ligne flexible mais avec une hauteur minimale de 35 mm afin d'avoir la place physique de signer.
* **Bas de page** : un simple bloc dont le contenu sera inséré en bas de page dans le PDF.

Prenons comme exemple la **ligne de signatures**. Vous pouvez créer autant de cases que vous le souhaitez en pressant *Entrée*. Si vous souhaitez aller à la ligne dans une même case, utilisez *Shift + Entrée*.

Chaque case peut être alignée à gauche, à droite ou centrée (utilisez les boutons d'alignement). Les flèches vertes vous indiquent l'alignement (droite et gauche pour le centrage).

Notez que les contours en pointillés comme les flèches n'apparaissent pas dans le document final. Ce ne sont que des repères pour vous aiguiller.

![](/images/modele-ligne-signatures.png)

La ligne **bas de page** est un simple cadre, dont le contenu sera inséré en pied de page.

![](/images/modele-note-bas-de-page.png)
