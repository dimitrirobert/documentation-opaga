# Utilisez des mots-clés

Vos modèles doivent être remplis avec des informations collectées auprès des clients, des stagiaires voire définies au niveau des sessions. pour tous ces champs utilisés les mots-clés à votre disposition à droite de l'éditeur de texte. Ils sont rangés par entité.

Certains mots-clés peuvent accepter un paramètre (cela est indiqué lorsque vous passez la souris sur le bouton du mot-clé) comme par exemple `of:logo:30` qui affiche le logo de l'OF avec une largeur de 30mm.

Il n'est pas possible pour vous d'ajouter de nouveaux mots-clés permettant d'extraire des données depuis la base, à moins de modifier le code source d'OPAGA. Si vous avez besoin de nouveaux mots-clés, n'hésitez pas à prendre contact avec l'auteur.

## Insérez un mot-clé

La liste des mots-clés se trouve à droite de la zone de saisie du texte. ils sont classés par entité où se trouve la donnée (par exemple, toutes les informations de tarif se trouvent au niveau du client). Cliquez sur le nom d'une entité pour ouvrir la sous-liste.

Les noms peuvent paraître un peu abscons parfois. Si c'est le cas, passez la souris sur un mot-clé et l'infobulle vous donnera plus de détails.

![](/images/modele-liste-motscles.png)

Cliquez sur le bouton du mot-clé pour l'ajouter dans l'éditeur de texte, à l'endroit où se trouve le curseur (pensez à bien positionner votre curseur avant).

Notez que les accolades apparaissent dans l'éditeur pour rendre visible les limites du mot-clé. Elles ne sont pas présentes dans le texte, c'est seulement pour l'affichage dans l'éditeur.

```admonish note title="Structure technique d'un mot-clé"

Un mot-clé est constitué du nom de l'entité support suivi d'un deux-points suivi de la donnée recherchée. Ce mot-clé est inséré dans le modèle avec un style nommé `keyword` affiché dans l'éditeur en rouge et entouré d'accolades (notez que les accolades ne sont pas dans le texte, seulement dans le style). Ce style est indispensable pour la détection des mots-clés lors de la création du document.
	
Veillez à toujours bien conserver ces styles ! Il vaut mieux effacer un mot-clé qui vous parait défectueux puis le recréer plutôt que d'essayer de le réparer.
```
	
## Paramétrez un mot-clé

Certains mots-clés acceptent un paramètre pour modifier leur comportement. Cette information est présente dans l'infobulle.

Par exemple, pour les tarifs et autres montants, vous ajouter les paramètres suivants :

* `ht` : le montant sera affiché hors taxes (comportement par défaut) ;
* `ttc` : le montant est affiché avec la TVA, si toutefois votre organisme est concerné (sinon, la valeur sera la même que HT) ;
* `tva` : affiche le montant de la TVA.

Ainsi, à partir du mot-clé `client:tarif_total_chiffre` il faut ajouter `:ttc` à la fin pour avoir le montant en TTC.

Voici un exemple :

> En contrepartie de cette action de formation, l’employeur s’acquittera des coûts suivants :
>
> Coût pédagogique par stagiaire : `client:tarif_stagiaire:ht` € HT
>
> Soit un total de : `client:tarif_total_chiffre:ht` € HT plus TVA à `client:tarif_total_chiffre:tva` (`of:tauxtva`%)
>
> TOTAL GÉNÉRAL : `client:tarif_total_chiffre:ttc` € TTC
