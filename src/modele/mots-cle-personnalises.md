# Mots-clé personnalisés

Si vous ne pouvez créer vous-même de mot-clé traités automatiquement, vous pouvez cependant créer des mots-clés personnalisés. Ces derniers sont obligatoirement rattachés à l'entité **Document** et nécessiteront une action de la part du/de la formatrice ou toute autre personne créant les documents : un formulaire permet, pour chaque document dont le modèle contient des champs personnalisés, de renseigner les informations demandées (voir [Complétez les paramètres personnalisés](../document/param-personnalises.md)).

Dans l'entité **Document** utilisez le mot-clé `document:personnalisable` puis remplacez `personnalisable` par ce que vous souhaitez. Notez que tant qu'il n'est pas reconnu comme un mot-clé personnalisé, le fond du texte est gris.

Enregistrez votre modèle via la boîte WordPress **Publier** (bouton **Publier** ou **Mettre à jour**). Au rechargement de la page vous voyez apparaître un nouveau bloc dans les options.

![](/images/modele-param-mot-cle-personnalise.png)
