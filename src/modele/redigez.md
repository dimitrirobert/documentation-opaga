## Rédigez

Vous pouvez rédiger directement dans l'éditeur de texte correspondant à la partie concernée. Cependant, il est plus probable que vous ayez déjà des modèles de document au format traitement de texte. Dans ce cas, vous pouvez copier-coller leurs contenus.

```admonish warning title="Attention au copié-collé"

La copie depuis un traitement de texte importe également tout un tas de styles qui vont perturber le rendu des documents PDF. C'est pourquoi est activé par défaut le bouton **Coller en texte** (icône rouge avec un T). Vous devrez donc refaire la mise en forme de votre texte.
```
