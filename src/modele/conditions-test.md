# Insérez des conditions de test

Pour ajouter un peu de souplesse dans vos modèles vous pouvez utiliser des conditions de test. Par exemple, si telle valeur vaut tant, alors on affiche tel tel texte, sinon tel autre.

Comme exemple concret, prenons la convention de formation. Parfois il est nécessaire de faire apparaître les noms des stagiaires, parfois ce n'est pas utile (voire compliqué si tous les noms ne sont pas connus au moment de la signature).

Pour cela procédez comme suit : ouvrez le bloc de mots-clés **Tests et conditions** puis insérez le mot-clé **Est vrai**.

![](/images/modele-inserer-test.png)

Le mot-clé est inséré, il faut en adapter le contenu. Un mot-clé de test est composé de trois parties séparées par le caractère barre verticale `|` :

* la condition de test qui se décompose elle-même en :
	- operande1
	- un éventuel signe de comparaison
	- operande2
* le texte affiché si la condition est validée
* le texte affiché si la condition n'est pas valide

Les textes à afficher sont facultatifs : on peut choisir de ne rien afficher si la condition n'est pas valide.

![](/images/modele-test-a-modifier.png)

```admonish info title="Les signes de comparaison"

Voici, pour information, la liste des signes de comparaisons, directement issus du langage PHP.

* `==` : est égal à
* `>` : est supérieur à
* `<` : est inférieur à
* `>=` : est supérieur ou égal à
* `<=` : est inférieur ou égal à
* `!=` : est différent de

Pas de signe pour tester si un opérande est vrai ou faux. Si l'opérande testé est vide ou égal à 0, il est considéré comme faux.
```

Il n'existe pas de mot-clé pour déterminer si les noms des stagiaires doivent être inclus ou non. Il faut donc créer un mot-clé personnalisé.

Sélectionnez le texte `operande1 operande2` et insérez le mot-clé `document:personnalisable` (dans un test vrai ou faux, il n'y a qu'un seul [opérande](https://fr.wikipedia.org/wiki/Opérande)).

![](/images/modele-inserer-test-mot-cle-personnalisable.png)

Remplacez `personnalisable` par ce que vous voulez. Disons `affiche les stagiaires`. Ne vous inquiétez pas des espaces et accents éventuels, le mot-clé est nettoyé automatiquement. Mettez à jour (enregistrez) le modèle depuis la boîte **Publier** puis **Mettre à jour**.

La boîte **Mot-clés personnalisés** apparaît sous les options du modèle.

![](/images/modele-param-mot-cle-personnalise.png)

Définissez avec précision la description : c'est elle qui est affichée dans le formulaire demandant aux formateur⋅trices de définir une valeur pour ce mot-clé. Il faut donc être précis.

Il s'agit ici d'une question dont la réponse sera oui ou non. Vous pouvez préciser une valeur par défaut ou non.

Enfin, le mot-clé ainsi défini apparaît désormais dans la liste des mot-clés de l'entité **Document** pour ce modèle de document uniquement.
