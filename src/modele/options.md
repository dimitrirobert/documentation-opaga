# Options de modèle

Le premier bloc vous permet de définir certaines options dont le contexte d'utilisation du document :

* type de contrat, direct ou en sous-traitance ;
* l'entité concernée (il peut y en avoir plusieurs), c'est-à-dire dans quel onglet sera géré ce document ;
* qui doit signer (sachant qu'actuellement, une signature par le responsable bloque la création du document final par le formateur).

Par exemple, la convention de formation est un contrat géré en direct, pour une personne morale. Elle doit apparaître au niveau du client et doit être signée par le responsable et le client au minimum (vous pouvez également envisager une signature par le formateur).

Autre exemple, le contrat de formation à titre individuel est un contrat géré en direct, pour une personne physique. Le destinataire est à la fois client et stagiaire. Il apparaît au niveau du client.

Ainsi que d'autres options :

* l'orientation du papier ;
* peut-être d'autres…

![](/images/modele-options.png)

