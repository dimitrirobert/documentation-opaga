# Documents administratifs

Dans la partie **Documents administratifs pour la session** sont listés les documents concernant le contexte « stagiaire », c'est-à-dire que vous en aurez un pour chaque client. La liste de ces documents est gérée au niveau de chaque organisme de formation dans la section [Modèles](../modele/index.md) dans le *backoffice*.

Voir comment [gérer les documents administratifs](../document/index.md).

<!-- TODO capture avec docs stagiaire -->

