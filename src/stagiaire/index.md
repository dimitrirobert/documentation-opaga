# Gérer les stagiaires

```admonish important
Vous devez obligatoirement inscrire des stagiaires sauf si [votre client organise lui-même la session de formation](../client/sous-traitance.md) (vous êtes sous-traitant de ce client ou un [particulier](../client/particulier.md) (client st stagiaire ne font qu'un).
```

- [Inscrivez vos stagiaires](./inscrire-stagiaires.md)
- [Tableau de bord stagiaire](./tableau-de-bord.md)
- [Gérez les documents administratifs](./docs-admin.md)
- [Suivi de présence](./presence.md)
- [Recueil des attentes](./attentes.md)
