# Renseigner chaque stagiaire

Chaque stagiaire dispose de son tableau de bord.

![](/images/stagiaire-tableau-de-bord.png)

Pour le bilan pédagogique et financier, vous devez renseigner le statut (ou type) du stagiaire parmi les choix suivants :

* Salarié d'employeur privé hors apprentis
* Apprenti
* Personne en recherche d'emploi formée par votre organisme de formation
* Particulier à ses propres frais formé par votre organisme de formation
* Autres stagiaires

La dernière catégorie permet de classer tous les statuts qui ne rentrent pas dans les autres tels que les agents du public (hors apprentis), les dirigeants non-salariés, les bénévoles.

Pour le BPF il est également nécessaire de vérifier que le nombre d'heures indiqué est bien celui effectué par le⋅la stagiaire. Si ce n'est le cas, vous devez le modifier dans le champ **Durée estimée en heures (décimal)**.
