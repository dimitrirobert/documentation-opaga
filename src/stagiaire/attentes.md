# Recueil des attentes

La dernière partie du tableau de bord stagiaire donne accès au recueil des besoins et attentes (modifiable par le stagiaire comme par le formateur) et aux résultats des évaluations.

La partie **attentes, besoins et motivations** est totalement libre. Le⋅la stagiaire peut ainsi s'exprimer sans cadre. Ce recueil peut également être fait par le⋅la formateur⋅trice lors d'un entretien téléphonique à l'issue duquel les deux parties valident le texte. Il s'agit du critère 4 de Qualiopi.

Ce recueil d'infirmations permet d'adapter la prestation pour mieux coller aux attentes (critère 10 de Qualiopi). L'idée étant, pour chaque stagiaire, d'atteindre les objectifs de la formation. Ainsi que favoriser l'engagement des bénéficiaires et prévenir les ruptures de parcours (critère 12 de Qualiopi).

Les évaluations telles que gérées actuellement permettent l'auto-positionnement des stagiaires sur des compétences relevant des pré-requis et des objectifs pédagogiques. L'évaluation des objectifs est proposée deux fois (avant et après la session de formation) afin d'évaluer la progression.

Cette partie évaluations va fortement évoluer afin d'offrir plus de souplesse et de possibilités.

<!-- TODO évaluations à revoir -->

![](/images/stagiaire-attentes-evaluations.png)