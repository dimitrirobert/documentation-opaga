# Inscrire des stagiaires

Depuis le tableau de bord d'un client vous pouvez ajouter un ou plusieurs stagiaires. Sous la zone **Pour infos** vous trouverez le bouton **Ajouter stagiaire**.

![](/images/client-ajouter-stagiaire-bouton.png)

Vous ouvrez ainsi une fenêtre qui vous permet de saisir les stagiaires un par un. Cette fenêtre est volontairement sobre car nous ne disposons pas toujours des informations complètes tout de suite. Comme pour un client, il y a lieu de compléter la fiche à posteriori.

![](/images/stagiaire-nouveau.png)

Lorsque vous validez un stagiaire (bouton **Valider** ou touche **Entrée**), la fenêtre reste ouverte et le formulaire est nettoyé pour vous permettre de saisir plusieurs stagiaires d'affilée. Vous devez cliquer sur **Fermer** lorsque vous avez fini.

À noter que si votre OPAGA est configuré pour conserver le genre des stagiaires, il vous sera demandé dans cette fenêtre.

```admonish note title="Importer des stagiaires depuis un tableur"

Vous vous demandez peut-être pourquoi ne peut-on importer une liste de stagiaires depuis un fichier CSV (format tableur minimaliste et universel). L'idée a été évoquée et se concrétisera sans doute un jour.

Une autre fonctionnalité à venir offrira la possibilité au client de renseigner lui-même ses stagiaires.
 
Pour l'heure, j'ai essayé de proposer une interface la plus simple possible tout en sachant que vous devrez saisir vos stagiaires au moins une fois, que ce soit dans OPAGA ou dans un tableur. Pourquoi ne pas le faire directement dans OPAGA ? Surtout qu'il est possible d'[exporter les informations au format CSV](export-donnees.md) ensuite.
```

