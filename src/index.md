# OPAGA

*Cette page compile toute la documentation disponible sur OPAGA et est donc susceptible d'évoluer souvent.*

## Fonctionnement général

En tant que formateur⋅trice vous allez :

* Produire des fiches de formation, dites *formation sur étagère*, des programmes génériques avec un tarif indicatif. Dans la pratique, vous ne vendrez jamais (ou très rarement) cette prestation telle quelle. Ces fiches vous servent à communiquer, à être visible publiquement (même si vous conservez le choix de ne pas les rendre publiques). Elle servent aussi de base pour programmer des sessions dont le programme sera personnalisé (dans la logique Qualiopi vous devrez toujours vous adapter aux attentes de vos clients).
* Programmer des sessions de formation.
	- des sessions en intra-entreprise à la demande d'un client. Ces sessions n'ont pas vocation à être publiques, donc pas visibles dans l'agenda.
	- des sessions en inter-entreprise publiées sur le site afin d'attirer les prospects.
	- des sessions en sous-traitance, non publiques, dont la vocation est surtout de noter les quelques informations nécessaires au bilan pédagogique et financier.
	
OPAGA ne propose pas cette distinction entre inter, intra et sous-traitance car trop restrictive. Le fonctionnement général est de :

* choisir la visibilité de la session dans l'agenda ;
* ajouter un ou des clients : si un seul client on est sur le l'intra, si un client est un autre organisme de formation alors, vous êtes sous-traitant. Mais vous pouvez, dans une même session, avoir des clients en direct (inter) et de la sous-traitance parce qu'un autre OF vous apporte des stagiaires.
* pour chaque client vous pouvez ensuite inscrire un ou des stagiaires (sauf si le client est un particulier alors, il est le seul stagiaire, et si le client est un OF vous n'avez qu'à noter le nombre de stagiaires et d'heures).

## Fonctionnalités

OPAGA est le logiciel libre de gestion administrative de la formation professionnelle.

Si vous êtes **formateur ou formatrice**, OPAGA vous permet de :

* produire votre catalogue de formations (fiches génériques présentant vos parcours de formation) ;
* renseigner votre profil de formateur (CV, description, références, etc.) ;
* programmer vos sessions de formation (événements avec dates et lieu) ;
* gérer vos clients et renseigner toutes les informations financières ;
* gérer les stagiaires de vos clients ;
* gérer les évaluations de vos stagiaires ;
* produire facilement les documents administratifs et les faire valider par votre responsable de formation ;
* enregistrer vos actions de veille (métier et compétences, juridique et administratif, pédagogique) ;
* exporter des données vers d'autres logiciels (au format tableur).

Si, vous êtes **responsable de formation**, OPAGA vous permet de :

* gérer l'équipe pédagogique (créer les formateurs, les activer) ;
* gérer les informations des lieux habituels de formation ;
* avoir une vue d'ensemble sur les sessions sur une période donnée ;
* produire le bilan pédagogique et financier ;
* gérer les options générales ;
* gérer la mise en page des documents PDF.

OPAGA est une extension pour WordPress.

Pour commencer, [connectez-vous à OPAGA](connexion.md) !

```admonish info
**Rôles utilisateurs**

Il existe actuellement deux rôles d'utilisateurs :

* **responsable de formation** qui permet de tout gérer dans OPAGA, les options, comme les contenus. Les responsables peuvent créer des comptes responsable et de formateur. Cependant la configuration de WordPress est dévolue  au rôle administrateur de WordPress.
* **formateur⋅trice** qui permet de rédiger des fiches formations et de programmer des sessions.
```

Cette documentation peut être reproduite, modifiée à partir de sa source disponible sur : <https://framagit.org/dimitrirobert/documentation-opaga/>
