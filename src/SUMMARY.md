# Summary

[Qu'est-ce qu'OPAGA ?](./index.md)
[Connexion](./connexion.md)

# Formateur⋅trice

- [Vous animez des formations](./formateur/index.md)
- [Tableau de bord](./formateur/tableau-de-bord.md)
    - [Profil de formateur](./formateur/infos-formateur.md)
    - [Rédigez un programme de formation](./formateur/programme-formation.md)
    - [Programmez une session de formation](./session/programmer-session.md)
    - [Veille](./formateur/veille.md)
- [Gérez les sessions](./session/index.md)
    - [Définissez le planning](./session/planning.md)
    - [Finalisez](./session/finaliser.md)
- [Gérez les clients](./client/index.md)
    - [Créez un client](./client/nouveau-client.md)
    - [Tableau de bord client](./client/tableau-de-bord.md)
    - [Financement et tarif](./client/financement.md)
    - [Gérez les documents administratifs](./client/docs-admin.md)
    - [Créneaux contractualisés](./client/creneaux.md)
    - [Le client est un particulier](./client/particulier.md)
    - [Vous êtes sous-traitant](./client/sous-traitance.md)
- [Gérez les stagiaires](./stagiaire/index.md)
    - [Inscrivez vos stagiaires](./stagiaire/inscrire-stagiaires.md)
    - [Tableau de bord stagiaire](./stagiaire/tableau-de-bord.md)
    - [Gérez les documents administratifs](./stagiaire/docs-admin.md)
    - [Suivi de présence](./stagiaire/presence.md)
    - [Recueil des attentes](./stagiaire/attentes.md)
- [Gérez les documents administratifs](./document/index.md)
    - [Personnalisez certains paramètres](./document/param-personnalises.md)
    - [Repérez les informations manquantes](./document/infos-manquantes.md)
- [Exportez des données](./formateur/export-donnees.md)

# Responsable de formation

- [Vous gérez l'organisme](./responsable/index.md)
    - [Validez les documents](./responsable/valider-document.md)
    - [Équipe pédagogique](./responsable/equipe-pedagogique.md)
    - [Bilan pédagogique et financier](./responsable/bpf.md)

# Paramétrage

- [Options](./params/index.md)
    - [Votre organisme](./params/opt-of.md)
    - [Tâches](./params/opt-taches.md)
    - [OPAGA](./params/opt-opaga.md)
    - [Champs prédéfinis](./params/opt-champs-predefinis.md)
    - [Options PDF](./params/opt-pdf.md)
    - [Configuration avancée](./params/opt-avance.md)

- [Modèles de document](./modele/index.md)
    - [Options de modèle](./modele/options.md)
    - [Rédigez](./modele/redigez.md)
    - [Utilisez des mots-clé](./modele/mots-cle.md)
    - [Créez des mots-clé personnalisés](./modele/mots-cle-personnalises.md)
    - [Insérez des conditions de test](./modele/conditions-test.md)
    - [Insérez des lignes formatées](./modele/lignes-formatees.md)
    - [Feuilles d'émargement](./modele/emargement.md)
- [Site public](./public/index.md)
    - [Utilisez des shortcodes](./public/shortcodes.md)
    - [Afficher le catalogue](./public/afficher-catalogue.md)

# Annexes

- [FAQ](./faq.md)
    - [Qu'est-ce qu'une formation ?](./faq/01-quest-ce-quune-formation.md)                                                                                                                                           
    - [Qu'est-ce qu'une session ?](./faq/02-quest-ce-quune-session.md)                                                                                                                                               
    - [Je ne travaille qu'en sous-traitance, que dois-je remplir ?](./faq/03-sous-traitance-seulement.md)                                                                                                            
    - [Lors d'un contrat avec un client qui vous confie plusieurs groupes à des moments différents, dois-je créer plusieurs sessions ?](./faq/50-client-multi-groupe.md)
    - [J'ai perdu ma session !](./faq/50-session-perdue.md)
- [Glossaire](./glossaire.md)
    - [Formation en inter-entreprises](./glossaire/inter-entreprises.md)
    - [Formation en intra-entreprise](./glossaire/intra-entreprise.md)



