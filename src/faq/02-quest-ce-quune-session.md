# Qu'est-ce qu'une session ?

Dans OPAGA le terme **Session** désigne un événement durant lequel vous transmettez ou échangez des compétences à un public, une *action de formation*. Selon les cas, elle peut s'appuyer sur une fiche de formation. Si **vous êtes sous-traitant** du seul client de cette session, vous n'avez pas besoin de vous appuyer sur une fiche de formation.

* Elle concerne un ou plusieurs clients (donc autant de contrats).
* Elle comporte une ou des dates. Vous devez mentionner toutes les dates de votre parcours de formation.
* Elle comporte un ou plusieurs lieux.

Il n'y a pas dans OPAGA de notion d'inter-, intra-entreprise ou de sous-traitance. C'est le nombre et/ou la typologie de vos clients qui définit ces aspects. Si votre client est un organisme de formation qui organise la présente session (ou au minimum qui gère tout ou partie des stagiaires), alors vous êtes sous-traitant pour ce client. Mais rien ne vous empêche, pour la même session, d'avoir également un ou plusieurs clients en direct qui vous confient chacun un ou plusieurs stagiaires. Ce qui constitue un subtil mélange de sous-traitance, inter et intra… Mais toutes les informations seront ensuite bien dispatchées.