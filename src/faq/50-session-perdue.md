# J'ai perdu ma session !

Je ne vois plus une session que j'anime dans la liste de mes sessions.

Je dois demander à un⋅e responsable de me la réaffecter (seules les personnes mentionnées dans l'équipe pédagogique et les responsables peuvent changer cela). Pour cela, iel doit :

* se rendre dans la page de gestion des sessions (agenda des sessions) ;
* chercher la session perdue (recherche sur le titre dans le champ de recherche en haut à droite du tableau) ;
* cliquer sur la colonne **Équipe pédagogique** de la session trouvée ;
* ajouter la ou les bonnes personnes.
