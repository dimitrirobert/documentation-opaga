# Lors d'un contrat avec un client qui vous confie plusieurs groupes à des moments différents, dois-je créer plusieurs sessions ?

Ça dépend, deux solutions sont possibles et permettent la gestion correcte des informations financières et pédagogiques.

* Une seule session, un seul client (à priori dans ce cas de figure, il n'y aura qu'un seul client) et tous les stagiaires ajoutés à ce client : il faudra ensuite distinguer les créneaux suivis par chaque stagiaire. Cela peut s'avérer assez lourd à gérer. Sauf lorsqu'une fonctionnalité de regroupement de stagiaires sera implémentée et la possibilité de définir les créneaux au niveau des groupes.
* Plusieurs sessions, un seul client dupliqué dans chaque session, mais autant de sessions que de groupes de stagiaires. Plus simple à gérer, mais le numéro de contrat, si vous l'utilisez, doit être dupliqué avec le client.
