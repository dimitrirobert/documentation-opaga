# Je ne travaille qu'en sous-traitance, que dois-je remplir ?

Il faut distinguer deux cas de figure.

1. **vous avez un ou plusieurs clients organismes qui gèrent les formations de A à Z, y compris le programme**
	* vous n'avez pas besoin de créer de programme de formation pour le catalogue
	* vous devez créer les sessions effectuées en sous-traitance avec un titre pertinent et les bonnes dates (que vous pouvez modifier à posteriori pour mieux coller à la réalité en cas de changement ou d'information incomplète à la signature de la convention). Pas besoin de détailler les créneaux horaires.
	* indiquez le numéro de déclaration d'activité du client (consultez [la liste des OF](https://inokufu.com/dashboard-mcf?tab=listof))
	* indiquez le nombre de stagiaires et le nombre d'heures × stagiaires (le cumul des heures suivies par chaque stagiaire)
	* indiquez le tarif facturé à l'organisme de formation (en précisant les frais non pédagogiques s'il y en a)
2. **vous êtes auteur⋅trice du programme et vous souhaitez en faire sa promotion pour éventuellement animer des sessions directement ou simplement pour chercher ou attirer d'autres organismes qui vous sous-traiteraient des actions**
	* vous devez rédiger la ou les programmes de formation correspondant. Vous n'êtes pas obligé de tout remplir si vous n'organisez jamais vous-même, donnez suffisamment de détails pour intéresser des organismes de formation.
	* vous créez les sessions à partir de vos programmes de formation
	* comme pour le cas 1, indiquez les dates (sans les créneaux), le numéro de déclaration d'activité, le nombre de stagiaires, le cumul d'heures de tous les stagiaires et le tarif.