# Qu'est-ce qu'une formation ?

Dans OPAGA le terme **Formation** est utilisé pour désigner un programme de formation à destination du catalogue et contenant un parcours pédagogique détaillé. Cette fiche doit satisfaire aux contraintes de l'indicateur 1 de Qualiopi portant sur l'*information accessible au public, détaillée et vérifiable sur les prestations proposées*. Il est donc impératif de bien remplir ce programme.
	
Attention, ce programme ne correspond pas à un événement. Vous ne devez mentionner ni dates, ni lieu (sauf si le lieu est induit par le contenu de la formation), ni client.

Vous devez rester généraliste et laisser la porte ouverte à la personnalisation. C'est ce que l'on appelle une *formation sur étagère*. Il y a peu de chance que vous la vendiez telle quelle. Ce n'est pas son but, c'est un *produit d'appel*.

Ce programme sert à :

* assurer la promotion de votre contenu de formation via la publicité du catalogue ;
* de support pour programmer une session de formation avec la possibilité de personnaliser son contenu à chaque fois.

Si vous ne **travaillez qu'en sous-traitance**, que les parcours de formation sont gérés par vos clients, que vous n'avez pas besoin de chercher de nouveaux clients, alors vous n'êtes pas obligé de remplir de programme de formation.
