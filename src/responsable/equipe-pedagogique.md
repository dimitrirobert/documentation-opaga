# Créer des comptes

```admonish important
Évitez de passer par la gestion des utilisateurs de WordPress.
```

Vous devez être administrateur ou responsable de formation.

Pour ajouter des utilisateurs, passez par votre tableau de bord (accessible en cliquant sur **Tableau de bord** dans le menu de droite) puis l'onglet **Comptes formeteurs/responsables**.

Le bouton + vous permet d'ajouter des lignes pour créer plusieurs comptes en une fois. Toutes les lignes n'ont pas à être remplie.

Remplissez tous les champs.

<video controls src="/video/ajout_utilisateurs.mp4"></video>

```admonish warning title="Attention"

Il n'est pas possible de créer deux utilisateurs ayant la même adresse email. Si plusieurs utilisateurs doivent absolument partager la même adresse insérez un suffixe dans la partie nom comme ceci : si l'adresse partagée est `contact@mail.com` vous pouvez inscrire les adresses `contact+1@mail.com`, `contact+celine@mail.com`, etc. tout ce que vous voulez entre + et @ du moment que ce sont des chiffres et des lettres.

Cette astuce n'est pas propre à OPAGA, elle est valable partout, y compris dans votre boîte mail.
```