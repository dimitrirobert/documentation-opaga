# Vous êtes responsable de formation

Vous avez donc un rôle de supervision et de configuration d'OPAGA.

## Équipe pédagogique

* [créez des comptes formateurs](./equipe-pedagogique.md)

## Suivi de l'activité de formation

* [bilan pédagogique et financier](./bpf.md)
* [validez les documents](./valider-document.md)

## Configuration d'OPAGA

* [description l'organisme de formation](../params/opt-of.md)
* [options spécifiques pour le fonctionnement d'OPAGA](../params/opt-opaga.md)
* [modèles de document](../modele/index.md)

## Site web public

* [affichez le catalogue](../public/afficher-catalogue.md)
* [utilisez des codes court](../public/shortcodes.md) pour afficher du contenu