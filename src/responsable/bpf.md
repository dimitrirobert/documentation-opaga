# Bilan pédagogique et financier

Le BPF est une obligation légale de tout organisme de formation. Il rend compte de l'activité de l'année précédente. Cette déclaration se fait généralement à partir du 1er avril (pour l'activité de l'année précédente) via [Mon activité formation](https://www.monactiviteformation.emploi.gouv.fr/mon-activite-formation/) sur le site du ministère du Travail.

Il existe toujours le formulaire [Cerfa 10443](https://www.formulaires.service-public.fr/gf/cerfa_10443.do) et [sa notice](https://www.formulaires.service-public.fr/gf/getNotice.do?cerfaNotice=50199&cerfaFormulaire=10443) qui vous seront d'une aide précieuse.

OPAGA est basé sur ce formulaire Cerfa.

## Votre BPF

Retrouvez, à tout moment, même avant la fin de l'année, votre BPF sur la page dédiée accessible depuis le menu de droite.

![](/images/bpf-acces-menu.png)

Cette page reprend la structure du Cerfa et donc toutes les informations que vous devez transmettre (sauf les informations qui n'ont pas lieu de se trouver dans OPAGA, telles que les salaires des formateurs, le chiffre d'affaire total de votre entreprise).

Toutes les informations compilées sur cette page proviennent des différentes sessions de l'année choisie[^1], des clients et des stagiaires.

![](/images/bpf-exemple.png)

## À la recherche des informations manquantes

Bien sûr, dans un monde idéal, le BPF serait prêt à copier-coller sur le site du ministère. Dans la vraie vie, il manque toujours des informations.

S'il y a des informations manquantes, elles sont recensées en rouge. De plus, en pied de chaque cadre un bouton vous permet d'ouvrir le détail des informations saisies, et surtout, celles non saisies.

Ainsi vous pourrez corriger, directement sur la page du BPF, ces informations.

![](/images/bpf-verif.png)

Vous pouvez également utiliser les pages de gestion pour faire vos corrections.

## URL et titre de la page du BPF

Si d’aventure vous souhaitiez modifier ou l'URL, ou le titre de cette page, rendez-vous dans les [options d'OPAGA](../params/opt-opaga.md#pages-speciales).

[^1]: il est prévu de gérer la répartition par exercice comptable pour les sessions chevauchant plusieurs années.