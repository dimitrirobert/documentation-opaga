# Afficher le catalogue de formation


L'affichage du catalogue doit être public (sans authentification), il se fait donc via une page existante de votre WordPress.

Insérer le *shortcode* suivant dans la page où vous souhaitez l'afficher.

```
[liste_formation]
```

Il existe de base une page nommée « Formations » et donc l'URL est `formations`. Cette URL est liée dans le menu de droite sous l'intitulé « Catalogue de formations ».