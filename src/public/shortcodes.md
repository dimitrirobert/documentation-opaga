# Utiliser des codes courts (shortcodes)

## À quoi servent les codes courts

Dans WordPress, les *shortcodes* ou codes courts sont des mots-clés à insérer dans du contenu (article, page, etc.) qui déclenchent une fonction PHP, donc exécutent du code qui va afficher certaines données. Cela permet d'ajouter de la programmation dans une page Web, sans laisser la possibilité de faire exécuter n'importe quel bout de code qui compromettrait le site.

Les shortcodes sont rédigés ainsi :

Les codes mono-balise

    [nom_du_code param1="valeur 1" param2 param3]

Les codes à balises encadrantes :

    [nom_du_code param1="etc"]Du texte ou tout autre contenu visuel[/nom_du_code]

## Le code générique `opaga`

Le code `opaga` est mono-balise. Il sert à appeler une fonction d'un objet. Sa structure est un peu plus complexe, mais voici comment l'utiliser :

* le mot-clé `opaga` en premier ;
* l'entité depuis laquelle nous voulons extraire une donnée : of, formation, session ou formateur ;
* la méthode (fonction) de l'objet précité a appeler pour obtenir une information (dans la classe concernée les méthodes appelables commencent par `get_doc_` et sont initialement utilisée pour la production des documents PDF) ;
* le mot-clé `id` avec la valeur de l'identifiant de la formation ou de la session choisie :
	* pour les sessions et formations il est possible de donner directement l'URL permanente ;
	* pour les formateurs, il faut donner l'identifiant numérique précis ;
	* pour l'organisme de formation, pas besoin d'identifiant, il n'y en a qu'un.
* le mot-clé `options` pour donner d'éventuelles options.
* le mot-clé `link` pour ajouter un lien hypertexte sur le contenu renvoyé par le shortcode : si `link` vaut 1 le permalien de l'entité est utilisé (par exemple le lien vers la page présentant un formateur), et si `link` contient une URL valide, un lien est créé vers cette URL.

```admonish note
Si la combinaison de tous ces éléments ne permet pas de trouver l'information cherchée, rien n'est affiché.
```

**Exemples**

Je veux afficher le numéro de déclaration d'activité de l'organisme de formation

    [opaga of no_of]

Je veux afficher la présentation de la formation dont le permalien est `https://mon-of.coop/formation/rediger-en-markdown/`

    [opaga formation presentation id="https://mon-of.coop/formation/rediger-en-markdown/"]

Je veux afficher l'identité et la photo avec une largeur de 40 pixels du formateur n°2

    [opaga formateur identite id="2"]
    [opaga formateur photo id="2" options="40x"]

```admonish info
Les options dépendent des mots-clés. La plupart n'en ont pas. Un mot-clé servant à afficher une image peut prendre en option les dimensions en pixels séparées par un `x`. Par exemple : `40x50`.

Si l'une des dimensions manque, elle est calculée de sorte à conserver les proportions, ce qui est préférable pour conserver le ratio et ne pas déformer l'image.
```

Je veux afficher les mêmes informations avec un lien vers la page présentant le formateur.

    [opaga formateur identite id="2" link=1]
    [opaga formateur photo id="2" options="40x" link=1]

Je veux afficher le logo de l'organisme avec un lien pointant vers `https://macooperative.coop/formations/`

    [opaga of logo link="https://macooperative.coop/formations/"]


## Autres codes