# Connexion

Trouvez le lien de connexion pour vous authentifier sur WordPress. La présentation dépend de la mise en forme de votre site de formation : par exemple, chez Coopaname, le lien de connexion est dans la barre de menu.

Cela vous mène sur la page dont l'URL est `/login/` :

![](/images/boite-connexion.png)

Saisissez votre identifiant ou votre adresse email, ainsi que votre mot de passe.

```admonish warning
si vous n'avez pas encore de mot de passe ou que vous l'avez perdu, utilisez le lien *Mot de passe oublié ?* que vous conduit sur la page dont l'URL est `/password-reset/`.
```

Vous arrivez ensuite sur votre [tableau de bord personnel](./formateur/tableau-de-bord.md). Ici, l'exemple du tableau de bord d'un formateur.

![](/images/tableau-bord-formateur.png)